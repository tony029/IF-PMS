v2.0
===
数据库：MySQL<br>
WEB框架：SpringMVC<br>
ORM框架：Nutz.Dao<br>
模板引擎：Beetl<br>
HTML前端模板：INSPINIA2.4（请购买正版授权）<br>

v1.0
===

WEB应用框架采用SpringMVC。<br>
ORM框架采用Nutz.Dao。<br>
模板引擎采用Bteel。<br>
前端使用H-ui.admin。<br>

演示地址:http://www.xn-ck.com:8080/

文档说明：http://www.blackzs.com/archives/519