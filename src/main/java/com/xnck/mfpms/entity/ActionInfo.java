/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.*;

import java.util.Date;
import java.util.List;

@Table("t_info_action")
@View("v_info_action")
public class ActionInfo
{
	public static final String FIELD_ID = "id";
    public static final String FIELD_PARENTID = "parentid";
    public static final String FIELD_DISPLAYNAME = "displayname";
	public static final String FIELD_PARENT = "parent";
    public static final String FIELD_ALL_CHILDS = "allchilds";
    public static final String FIELD_ALL_PARENTS = "allparents";
    public static final String FIELD_ROLES = "roles";
    public static final String FIELD_USERS = "users";
    public static final String FIELD_RESES = "reses";

	@Name
	private String id;
	@Column
	private String displayname;
	@Column
	private String parentid;
	@Column
	private String createrid;
	@Column
	private boolean enable;
	@Column
	private String remark;
	@Column
	private Date addtime;
    @Column
    @Readonly
    private int childcount;
    @Column
    @Readonly
    private String parentname;
    @One(target = ActionInfo.class, field = "parentid")
	private ActionInfo parent;
    @ManyMany(target = ActionInfo.class, relation = "t_action_child", from = "parentid", to = "childid")
	private List<ActionInfo> allchilds;
    @ManyMany(target = ActionInfo.class, relation = "t_action_child", from = "childid", to = "parentid")
    private List<ActionInfo> allparents;
	@ManyMany(target = RoleInfo.class, relation = "t_role_action", from = "actionid", to = "roleid")
	private List<RoleInfo> roles;
    @ManyMany(target = UserInfo.class, relation = "t_user_action", from = "actionid", to = "userid")
    private List<UserInfo> users;
    @ManyMany(target = ResourceInfo.class, relation = "t_action_resource", from = "actionid", to = "resourceid")
    private List<ResourceInfo> reses;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public String getCreaterid() {
		return createrid;
	}

	public void setCreaterid(String createrid) {
		this.createrid = createrid;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getAddtime() {
		return addtime;
	}

	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}

    public int getChildcount() {
        return childcount;
    }

    public void setChildcount(int childcount) {
        this.childcount = childcount;
    }

    public String getParentname() {
        return parentname;
    }

    public void setParentname(String parentname) {
        this.parentname = parentname;
    }

    public List<ActionInfo> getAllchilds() {
        return allchilds;
    }

    public void setAllchilds(List<ActionInfo> allchilds) {
        this.allchilds = allchilds;
    }

    public List<ActionInfo> getAllparents() {
        return allparents;
    }

    public void setAllparents(List<ActionInfo> allparents) {
        this.allparents = allparents;
    }

    public ActionInfo getParent() {
        return parent;
    }

    public void setParent(ActionInfo parent) {
        this.parent = parent;
    }

    public List<RoleInfo> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleInfo> roles) {
        this.roles = roles;
    }

    public List<UserInfo> getUsers() {
        return users;
    }

    public void setUsers(List<UserInfo> users) {
        this.users = users;
    }

    public List<ResourceInfo> getReses() {
        return reses;
    }

    public void setReses(List<ResourceInfo> reses) {
        this.reses = reses;
    }
}