/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Table;

@Table("t_role_action")
@PK({"roleid","actionid"})
public class RoleAction
{
	public static final String FIELD_ROLEID = "roleid";
    public static final String FIELD_ACTIONID = "actionid";

	private String roleid;
	private String actionid;

    public String getRoleid()
	{
		return roleid;
	}
	public void setRoleid(String roleid)
	{
		this.roleid=roleid;
	}
	public String getActionid()
	{
		return actionid;
	}
	public void setActionid(String actionid)
	{
		this.actionid=actionid;
	}

}