/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ManyMany;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.Table;

import java.util.Date;
import java.util.List;

@Table("t_info_role")
public class RoleInfo
{
    public static final String FIELD_ID = "id";
	public static final String FIELD_DISPLAYNAME = "displayname";
    public static final String FIELD_ACTIONS = "actions";
    public static final String FIELD_RULES = "rules";
    public static final String FIELD_MENUS = "menus";
    public static final String FIELD_USERS = "users";

	@Column
	@Name
	private String id;
	@Column
	private String displayname;
	@Column
	private String createrid;
	@Column
	private boolean enable;
	@Column
	private Date addtime;
	@ManyMany(target = ActionInfo.class, relation = "t_role_action", from = "roleid", to = "actionid")
    private List<ActionInfo> actions;
    @ManyMany(target = RuleInfo.class, relation = "t_role_rule", from = "roleid", to = "ruleid")
    private List<RuleInfo> rules;
    @ManyMany(target = MenuInfo.class, relation = "t_role_menu", from = "roleid", to = "menuid")
    private List<MenuInfo> menus;
	@ManyMany(target = UserInfo.class, relation = "t_user_role", from = "roleid", to = "userid")
	private List<UserInfo> users;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getCreaterid() {
		return createrid;
	}

	public void setCreaterid(String createrid) {
		this.createrid = createrid;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public Date getAddtime() {
		return addtime;
	}

	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}

    public List<ActionInfo> getActions() {
        return actions;
    }

    public void setActions(List<ActionInfo> actions) {
        this.actions = actions;
    }

    public List<RuleInfo> getRules() {
        return rules;
    }

    public void setRules(List<RuleInfo> rules) {
        this.rules = rules;
    }

    public List<MenuInfo> getMenus() {
        return menus;
    }

    public void setMenus(List<MenuInfo> menus) {
        this.menus = menus;
    }

    public List<UserInfo> getUsers() {
        return users;
    }

    public void setUsers(List<UserInfo> users) {
        this.users = users;
    }
}