/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.*;

import java.util.Date;
import java.util.List;

@Table("t_info_rule")
@View("v_info_rule")
public class RuleInfo
{
    public static final String FIELD_ID = "id";
    public static final String FIELD_DISPLAYNAME = "displayname";
    public static final String FIELD_PARENTID = "parentid";
    public static final String FIELD_PARENT = "parent";
    public static final String FIELD_ALL_CHILDS = "allchilds";
    public static final String FIELD_ALL_PARENTS = "allparents";
    public static final String FIELD_ROLES = "roles";
    public static final String FIELD_USERS = "users";
    public static final String FIELD_QPARTS = "qparts";

	@Name
	private String id;
	@Column
	private String displayname;
	@Column
	private String parentid;
	@Column
	private String createrid;
	@Column
	private boolean enable;
	@Column
	private Date addtime;
    @Column
    @Readonly
    private String parentname;
    @Column
    @Readonly
    private int childcount;
    @One(target = RuleInfo.class, field = "parentid")
    private RuleInfo parent;
    @ManyMany(target = RuleInfo.class, relation = "t_rule_child", from = "parentid", to = "childid")
    private List<RuleInfo> allchilds;
    @ManyMany(target = RuleInfo.class, relation = "t_rule_child", from = "childid", to = "parentid")
    private List<RuleInfo> allparents;
    @ManyMany(target = RoleInfo.class, relation = "t_role_rule", from = "ruleid", to = "roleid")
    private List<RoleInfo> roles;
    @ManyMany(target = UserInfo.class, relation = "t_user_rule", from = "ruleid", to = "userid")
    private List<UserInfo> users;
    @ManyMany(target = QueryPart.class, relation = "t_rule_query_part", from = "ruleid", to = "querypartid")
    private List<QueryPart> qparts;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public String getCreaterid() {
		return createrid;
	}

	public void setCreaterid(String createrid) {
		this.createrid = createrid;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public Date getAddtime() {
		return addtime;
	}

	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}

    public String getParentname() {
        return parentname;
    }

    public void setParentname(String parentname) {
        this.parentname = parentname;
    }

    public int getChildcount() {
        return childcount;
    }

    public void setChildcount(int childcount) {
        this.childcount = childcount;
    }

    public List<RuleInfo> getAllchilds() {
        return allchilds;
    }

    public void setAllchilds(List<RuleInfo> allchilds) {
        this.allchilds = allchilds;
    }

    public List<RuleInfo> getAllparents() {
        return allparents;
    }

    public void setAllparents(List<RuleInfo> allparents) {
        this.allparents = allparents;
    }

    public RuleInfo getParent() {
        return parent;
    }

    public void setParent(RuleInfo parent) {
        this.parent = parent;
    }

    public List<RoleInfo> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleInfo> roles) {
        this.roles = roles;
    }

    public List<UserInfo> getUsers() {
        return users;
    }

    public void setUsers(List<UserInfo> users) {
        this.users = users;
    }

    public List<QueryPart> getQparts() {
        return qparts;
    }

    public void setQparts(List<QueryPart> qparts) {
        this.qparts = qparts;
    }
}