/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.controller;

import com.xnck.mfpms.annotation.Permission;
import com.xnck.mfpms.entity.QueryPart;
import com.xnck.mfpms.exception.ValidateException;
import com.xnck.mfpms.service.QueryPartService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/sys/pms/qpart")
public class SysQueryPartController extends BaseController{

    private static final Logger log = Logger.getLogger(SysQueryPartController.class);

    @Autowired
    private QueryPartService partService;

    /**
     * 资源列表
     * @param request
     * @param response
     * @return
     */
    @Permission("url_sys_pms_qpart_list")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView getList(HttpServletRequest request, HttpServletResponse response){
        ModelAndView mv = new ModelAndView("/sys/pms/qpart/list");
        return mv;
    }

    /**
     * 资源总数
     * @param request
     * @param response
     * @param name
     */
    @Permission("url_sys_pms_qpart_list")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public void postList(HttpServletRequest request, HttpServletResponse response,
                         @RequestParam(value = "name")String name){
        try {
            int count = partService.getsCount(name);
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("count", count);
            responseSuccessMsg(response, map, "OK");
        }
        catch (Exception e){
            log.error(e.toString());
            responseErrorMsg(response, "内部异常");
        }
    }

    /**
     * 资源主体
     * @param request
     * @param response
     * @param pageSize
     * @param beginIndex
     * @param name
     * @param orderName
     * @param orderType
     * @return
     */
    @Permission("url_sys_pms_qpart_listbody")
    @RequestMapping(value = "/listbody", method = RequestMethod.POST)
    public ModelAndView postBody(HttpServletRequest request, HttpServletResponse response,
                                 @RequestParam(value = "limit")int pageSize,
                                 @RequestParam(value = "offset")int beginIndex,
                                 @RequestParam(value = "name")String name,
                                 @RequestParam(value = "sort", required = false)String orderName,
                                 @RequestParam(value = "order", required = false)String orderType){
        ModelAndView mv = new ModelAndView("/sys/pms/qpart/listbody");
        try {
            List<QueryPart> parts = partService.gets(name, orderName, orderType, pageSize, beginIndex);
            mv.addObject("parts", parts);
        }
        catch (Exception e){
            log.error(e.toString());
        }
        return mv;
    }

    /**
     * 创建资源
     * @param request
     * @param response
     * @return
     */
    @Permission("url_sys_pms_qpart_create")
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView getCreate(HttpServletRequest request, HttpServletResponse response){
        ModelAndView mv = new ModelAndView("/sys/pms/qpart/create");
        return mv;
    }

    /**
     * 创建资源
     * @param request
     * @param response
     */
    @Permission("url_sys_pms_qpart_create")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void postCreate(HttpServletRequest request, HttpServletResponse response,
                           @RequestParam(value = "displayname")String displayName,
                           @RequestParam(value = "partname")String partName,
                           @RequestParam(value = "queryname")String queryName,
                           @RequestParam(value = "enable", required = false)boolean enable){
        try {
            partService.create(displayName, partName, queryName, enable);
            responseSuccessMsg(response, null, "添加成功");
        }
        catch (ValidateException ve){
            log.error(ve);
            responseErrorMsg(response, ve.getMessage());
        }
        catch (Exception e){
            log.error(e);
            responseErrorMsg(response, "内部异常");
        }
    }

    /**
     * 修改资源
     * @param request
     * @param response
     * @return
     */
    @Permission("url_sys_pms_qpart_update")
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ModelAndView getUpdate(HttpServletRequest request, HttpServletResponse response,
                                  @RequestParam(value = "id")String partId){
        ModelAndView mv = new ModelAndView("/sys/pms/qpart/update");
        try {
            QueryPart part = partService.get(partId);
            mv.addObject("part", part);
        }
        catch (Exception e){
            log.error(e);
        }
        return mv;
    }

    /**
     * 修改资源
     * @param request
     * @param response
     */
    @Permission("url_sys_pms_qpart_update")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public void postUpdate(HttpServletRequest request, HttpServletResponse response,
                           @RequestParam(value = "id")String partId,
                           @RequestParam(value = "displayname")String displayName,
                           @RequestParam(value = "partname")String partName,
                           @RequestParam(value = "queryname")String queryName,
                           @RequestParam(value = "enable", required = false)boolean enable){
        try {
            partService.update(partId, displayName, partName, queryName, enable);
            responseSuccessMsg(response, null, "修改成功");
        }
        catch (ValidateException ve){
            log.error(ve);
            responseErrorMsg(response, ve.getMessage());
        }
        catch (Exception e){
            log.error(e);
            responseErrorMsg(response, "内部异常");
        }
    }

    /**
     * 批量删除
     * @param request
     * @param response
     */
    @Permission("url_sys_pms_qpart_multidel")
    @RequestMapping(value = "/multidel", method = RequestMethod.POST)
    public void postMulti(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam(value = "ids")String partIds){
        try {
            partService.deletes(partIds);
            responseSuccessMsg(response, null, "删除成功");
        }
        catch (Exception e){
            log.error(e.toString());
            responseErrorMsg(response, "内部异常");
        }
    }

    /**
     * 设置规则
     * @param request
     * @param response
     * @param qpartId
     * @return
     */
    @Permission("url_sys_pms_qpart_setrule")
    @RequestMapping(value = "/setrule", method = RequestMethod.GET)
    public ModelAndView getSetRule(HttpServletRequest request, HttpServletResponse response,
                                   @RequestParam(value = "id")String qpartId){
        ModelAndView mv = new ModelAndView("/sys/pms/qpart/setrule");
        try {
            QueryPart part = partService.get(qpartId);
            mv.addObject("part", part);
        }
        catch (Exception e){
            log.error(e);
        }
        return mv;
    }

    /**
     * 设置规则
     * @param request
     * @param response
     * @param qpartId
     * @param ruleIdStr
     */
    @Permission("url_sys_pms_qpart_setrule")
    @RequestMapping(value = "/setrule", method = RequestMethod.POST)
    public void postSetRule(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam(value = "id")String qpartId,
                            @RequestParam(value = "ids")String ruleIdStr){
        try {
            partService.joinRule(qpartId, ruleIdStr);
            responseSuccessMsg(response, null, "关联成功");
        }
        catch (ValidateException ve){
            log.error(ve);
            responseErrorMsg(response, ve.getMessage());
        }
        catch (Exception e){
            log.error(e);
            responseErrorMsg(response, "内部异常");
        }
    }

    /**
     * 规则树
     * @param request
     * @param response
     * @param qpartId
     */
    @Permission("url_sys_pms_qpart_rulenodes")
    @RequestMapping(value = "/rulenodes", method = RequestMethod.POST)
    public void postRuleNodes(HttpServletRequest request, HttpServletResponse response,
                              @RequestParam(value = "id")String qpartId){
        try {
            List<Map<String, Object>> nodes = partService.getRuleNodes(qpartId);
            responseSuccessMsg(response, nodes, "ok");
        }
        catch (ValidateException ve){
            log.error(ve);
            responseErrorMsg(response, ve.getMessage());
        }
        catch (Exception e){
            log.error(e);
            responseErrorMsg(response, "内部错误");
        }
    }
}
