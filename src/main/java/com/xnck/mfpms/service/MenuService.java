/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.service;

import com.xnck.mfpms.dao.MenuDao;
import com.xnck.mfpms.entity.MenuInfo;
import org.nutz.dao.Cnd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MenuService{

    @Autowired
    private MenuDao menuDao;

    /**
     * 获得人员所能访问的菜单
     * @param curUserId
     * @return
     */
    public Map<String, Object> getsByUserId(String curUserId){
        Map<String, Object> map = new HashMap<String, Object>();
        List<MenuInfo> menus = menuDao.getMenuInfoByUserRole(curUserId,
                Cnd.where(MenuInfo.FIELD_ENABLE, "=", true));
        for (MenuInfo menu : menus){
            map.put(menu.getId(), true);
        }
        menus = menuDao.getMenuInfoByUser(curUserId,
                Cnd.where(MenuInfo.FIELD_ENABLE, "=", true));
        for (MenuInfo menu : menus){
            if (!map.containsKey(menu.getId())) {
                map.put(menu.getId(), true);
            }
        }
        return map;
    }
}
