/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.enums;

public enum Operator {
    Eq,
    Gt,
    Lt,
    Like,
    In,
    NotIn,
    Or,
    And,
    Le,
    Ge,
    NotEq
}
