/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.dao;

import com.xnck.mfpms.entity.RoleRule;
import org.springframework.stereotype.Component;

@Component
public class RoleRuleDao extends BaseDao<RoleRule>{
}
