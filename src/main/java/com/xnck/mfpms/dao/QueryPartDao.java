/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.dao;

import com.xnck.mfpms.entity.QueryPart;
import org.nutz.dao.Condition;
import org.nutz.dao.sql.Sql;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class QueryPartDao extends BaseDao<QueryPart>{

    public QueryPart insert(String id, String displayName, String partName, String queryName, boolean enable){
        QueryPart queryPart = new QueryPart();
        queryPart.setEnable(enable);
        queryPart.setDisplayname(displayName);
        queryPart.setId(id);
        queryPart.setPartname(partName);
        queryPart.setQueryname(queryName);
        return this.dao.insert(queryPart);
    }

    /**
     * 规则已关联的资源数量
     * @param ruleId
     * @param condition
     * @return
     */
    public int getCountJoinRule(String ruleId, Condition condition) {
        Sql sql = dao.sqls().create("qpartDao.getCountJoinRule");
        sql.params().set("ruleid", ruleId);
        sql.setCondition(condition);
        return this.searchCount(sql);
    }

    /**
     * 规则已关联的资源
     * @param ruleId
     * @param condition
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<QueryPart> getJoinRule(String ruleId, Condition condition, int currentPage, int pageSize){
        Sql sql = dao.sqls().create("qpartDao.getJoinRule");
        sql.params().set("ruleid", ruleId);
        sql.setCondition(condition);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 规则未关联的资源数量
     * @param ruleId
     * @param condition
     * @return
     */
    public int getCountNotJoinRule(String ruleId, Condition condition) {
        Sql sql = dao.sqls().create("qpartDao.getCountNotJoinRule");
        sql.params().set("ruleid", ruleId);
        sql.setCondition(condition);
        return this.searchCount(sql);
    }

    /**
     * 规则未关联的资源
     * @param ruleId
     * @param condition
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<QueryPart> getNotJoinRule(String ruleId, Condition condition, int currentPage, int pageSize){
        Sql sql = dao.sqls().create("qpartDao.getNotJoinRule");
        sql.params().set("ruleid", ruleId);
        sql.setCondition(condition);
        return this.searchByPage(sql, currentPage, pageSize);
    }
}
