/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.dao;

import com.xnck.mfpms.entity.MenuInfo;
import org.nutz.dao.Condition;
import org.nutz.dao.sql.Sql;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MenuDao extends BaseDao<MenuInfo>{

    /**
     * 获取人员能够访问的菜单列表
     * @param curUserId
     * @param condition
     * @return
     */
    public List<MenuInfo> getMenuInfoByUserRole(String curUserId, Condition condition){
        Sql sql = this.dao.sqls().create("menuDao.getMenuInfoByUserRole");
        sql.setParam("userId", curUserId);
        sql.setCondition(condition);
        return this.search(sql);
    }

    /**
     * 获取人员能够访问的菜单列表
     * @param curUserId
     * @param condition
     * @return
     */
    public List<MenuInfo> getMenuInfoByUser(String curUserId, Condition condition){
        Sql sql = this.dao.sqls().create("menuDao.getMenuInfoByUser");
        sql.setParam("userId", curUserId);
        sql.setCondition(condition);
        return this.search(sql);
    }
}
