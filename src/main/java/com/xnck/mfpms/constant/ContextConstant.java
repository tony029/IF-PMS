/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.constant;

/**
 * 上下文常量
 *
 * @author zhangmengliang
 */
public class ContextConstant {

    /**
     * 人员身份凭证键名
     **/
    public static final String IDEN_CERT_KEY = "current_user_cert";
    /**
     * 验证码标识
     */
    public static final String VALIDAT_CODE_KEY = "validat_code";
    /**
     * 返回状态键名
     **/
    public static final String KEY_CODE = "code";
    /**
     * 返回数据键名
     **/
    public static final String KEY_DATA = "data";
    /**
     * 返回信息键名
     **/
    public static final String KEY_MSG = "msg";
    /**
     * 代表成功的值
     **/
    public static final String VALUE_SUCCESS = "200";
    /**
     * 代表错误的值
     **/
    public static final String VALUE_ERROR = "500";
    /**
     * SESSION超时
     */
    public static final String VALUE_SESSION_OUT = "301";
    /**
     * 身份认证失败
     */
    public static final String VALUE_AUTHEN_FAILED = "302";
}
