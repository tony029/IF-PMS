package com.xnck.mfpms.exception;

public class ValidateException extends Exception{
    public ValidateException(String message){
        super(message);
    }
}
